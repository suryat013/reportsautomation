import decimal
import json
import logging
import os
import shutil
import sys
import time
from datetime import datetime
from decimal import Decimal
from email import encoders
from email.mime.base import MIMEBase
import pandas as pd
from bs4 import BeautifulSoup
import pyodbc
import pkg_resources.py2_warn
from main import upload_results_to_test_rail
import requests


def write_date_to_file(file_path, data):
    with open(file_path, 'w') as file:
        if isinstance(data, str):
            file.write(data)
        else:
            json.dump(data, file, indent=4)
    file.close()


def dec_serializer(o):
    if isinstance(o, decimal.Decimal):
        return float(o)


def default(obj):
    if isinstance(obj, Decimal):
        return str(obj)
    raise TypeError("Object of type '%s' is not JSON serializable" % type(obj).__name__)


def main():
    logging.basicConfig(filename='error.log', level=logging.INFO)
    logging.info('Started')

    logging.info('Finished')


def objectmapping(obj, keyProp):
    return {obj[keyProp]: obj}


def default(obj):
    if isinstance(obj, Decimal):
        return str(obj)
    raise TypeError("Object of type '%s' is not JSON serializable" % type(obj).__name__)


def my_list_cmp(list1, list2):
    if (list1.__len__() != list2.__len__()):
        return False
    for l in list1:
        found = False
        for m in list2:
            res = my_obj_cmp(l, m)
            if (res):
                found = True
                break
        if (not found):
            return False
    return True


def my_obj_cmp(obj1, obj2):
    if isinstance(obj1, list):
        if (not isinstance(obj2, list)):
            return False
        return my_list_cmp(obj1, obj2)
    elif (isinstance(obj1, dict)):
        if (not isinstance(obj2, dict)):
            return False
        exp = set(obj2.keys()) == set(obj1.keys())
        if (not exp):
            # print(obj1.keys(), obj2.keys())
            return False
        for k in obj1.keys():
            val1 = obj1.get(k)
            val2 = obj2.get(k)
            if isinstance(val1, list):
                if (not my_list_cmp(val1, val2)):
                    return False
            elif isinstance(val1, dict):
                if (not my_obj_cmp(val1, val2)):
                    return False
            else:
                if val2 != val1:
                    return False
    else:
        return obj1 == obj2
    return True
# def my_list_cmp(list1, list2):
#     if (list1.__len__() != list2.__len__()):
#         return False
#     for l in list1:
#         found = False
#         for m in list2:
#             res = my_obj_cmp(l, m)
#             if (res):
#                 found = True
#                 break
#         if (not found):
#             return False
#     return True
# def my_obj_cmp(obj1, obj2):
#     if isinstance(obj1, list):
#         if (not isinstance(obj2, list)):
#             return False
#         return my_list_cmp(obj1, obj2)
#     elif (isinstance(obj1, dict)):
#         if (not isinstance(obj2, dict)):
#             return False
#         exp = set(obj2.keys()) == set(obj1.keys())
#         if (not exp):
#             # print(obj1.keys(), obj2.keys())
#             return False
#         for k in obj1.keys():
#             val1 = obj1.get(k)
#             val2 = obj2.get(k)
#             if isinstance(val1, list):
#                 if (not my_list_cmp(val1, val2)):
#                     return False
#             elif isinstance(val1, dict):
#                 if (not my_obj_cmp(val1, val2)):
#                     return False
#             else:
#                 import numbers
#                 if isinstance(val1,numbers.Number) and isinstance(val2, numbers.Number):
#                     if abs(val1 - val2) > 0.50:
#                         return False
#                 elif val2 != val1:
#                     return False
#     else:
#         import numbers
#         if isinstance(obj1,numbers.Number) and isinstance(obj2, numbers.Number) and abs(obj1 - obj2) < 0.50:
#             return True
#         return obj1 == obj2
#     return True


def get_decrypted_password(encrypted_password):
    try:
        import base64
        return base64.b64decode(encrypted_password).decode('utf-8')
    except Exception:
        print("Invalid database credentials!")
        raise


def email_report(message, subject, configData, global_file_name=''):
    import smtplib
    import email.utils
    from email.mime.multipart import MIMEMultipart
    from email.mime.text import MIMEText
    try:
        from_address = configData["EmailSender"]
        to_addresses = configData["EmailRecepients"]
        msg = MIMEMultipart()
        msg['From'] = email.utils.formataddr((configData["EmailSenderAlias"],
                                              configData["EmailSender"]))
        msg['To'] = ", ".join(to_addresses)
        msg['Subject'] = subject
        body = message
        msg.attach(MIMEText(body, 'html'))
        if global_file_name.endswith('.zip'):
            zf = open(global_file_name, 'rb')
            part = MIMEBase('application', "octet-stream")
            part.set_payload(zf.read())
            encoders.encode_base64(part)
            part.add_header('Content-Disposition', 'attachment; filename=' + global_file_name)
            msg.attach(part)
            zf.close()
        else:
            global_file_object = open(global_file_name, 'r')
            log_data = MIMEText(global_file_object.read())
            log_data.add_header('Content-Disposition', 'attachment', filename=global_file_name)
            msg.attach(log_data)
            global_file_object.close()
        server = smtplib.SMTP('smtp.gmail.com', 587)
        server.starttls()
        server.login(configData["EmailSender"], get_decrypted_password(configData["EmailSenderpassword"]))
        text = msg.as_string()
        server.sendmail(from_address, to_addresses, text)
        server.quit()
    except Exception as e:
        print(e)


def get_secrets(pod, secretname, type=''):  # pylint: disable=redefined-builtin
    from xmlrpc.client import ServerProxy
    client = ServerProxy("http://localhost:5999")
    return client.getsecrets(pod, secretname, type)


def proccheck(procedurename, connectioname):
    sqlPrepend = "set nocount on \n declare @procs table (Val nvarchar(256))"
    for procname in procedurename:
        sqlPrepend = sqlPrepend + "\n insert into " + '@procs' + " values (" + str(procname) + ")"
    query = sqlPrepend + "\n " + """select * from @procs pc where not exists (select * from sys.objects WHERE type = 'P' and name = pc.Val)"""
    # s"""select * from sys.objects WHERE type = 'P' and name not in (%(arg)s) """ %{'arg': ','.join(procedurename)}

    try:
        cursor = connectioname.cursor()
        result = connectioname.execute(query)
        data = result.fetchall()
        columns = [column[0] for column in result.description]
        results = []
        for row in data:
            results.append(dict(zip(columns, row)))
        cursor.close()
    except Exception as failure:

        logging.info("failure")
    return results


if __name__ == '__main__':
    main()
    with open(os.getcwd() + '//Data//' + 'Globalinputsjson.json', 'r') as globalinputs_data:
        globalPararmVals = json.load(globalinputs_data)
    with open(os.getcwd() + '//Data//' + 'Executer.json', 'r') as Executer_data:
        json_data = json.load(Executer_data)
    with open(os.getcwd() + '//Data//' + 'SCENARIO.json', 'r') as test_data:
        json_data1 = json.load(test_data)
    # with open(os.getcwd() + '//Data//' + 'Globalinputsjson.json', 'r') as globalinputs_data:
    #     globalPararmVals = json.load(globalinputs_data)
    with open(os.getcwd() + '//config//' + 'statusdetails.json', 'r')as statusdetails_data:
        statusDetails = json.load(statusdetails_data)
    statusDetails['IsBeforeRun'] = int(sys.argv[1])
    with open("statusdetails.json", "w") as json_file:
        json.dump(statusDetails, json_file)
    with open(os.getcwd() + '//Data//' + 'proc.json', 'r')as proc_data:
        proc = json.load(proc_data)

    with open(os.getcwd() + '//config//' + 'db.json', 'r') as dbdetails_data:
        dbdetails = json.load(dbdetails_data)
    pod, dbsecret = dbdetails["primarykey"].split('~')
    dbs = get_secrets(pod, dbsecret, 'MSSQL')
    dbdetails["primary"] = {}
    dbdetails["primary"]["server"] = dbs["SERVERNAME"]
    dbdetails["primary"]["name"] = dbs["DBNAME"]
    dbdetails["primary"]["username"] = dbs["USERNAME"]
    dbdetails["primary"]["password"] = dbs["PASSWORD"]
    pod, dbsecret = dbdetails["rptkey"].split('~')
    dbs = get_secrets(pod, dbsecret, 'MSSQL')
    dbdetails["replication"] = {}
    dbdetails["replication"]["server"] = dbs["SERVERNAME"]
    dbdetails["replication"]["name"] = dbs["DBNAME"]
    dbdetails["replication"]["username"] = dbs["USERNAME"]
    dbdetails["replication"]["password"] = dbs["PASSWORD"]

    with open(os.getcwd() + '//config//' + 'Output.json', 'r') as outputdetails_data:
        outputdetails = json.load(outputdetails_data)
    with open(os.getcwd() + '//config//' + 'runtype.json', 'r') as runtype_data:
        rundetails = json.load(runtype_data)
    with open(os.getcwd() + '//config//' + 'mailconfig.json', 'r') as mailconfig_data:
        mailconfig = json.load(mailconfig_data)

    runtype = rundetails["runtype"]
    isBeforeRun = statusDetails["IsBeforeRun"]
    globalParamKeys = globalPararmVals.keys()
    categories = objectmapping(json_data[0], "Category").keys()
    reportsByCategory = {}
    for categoryreports in json_data:
        detailsByReport = {}
        for proceducres in categoryreports["reports"]:
            parametersByName = {}
            for parameter in proceducres["parameters"]:
                parametersByName[parameter["name"]] = parameter
            detailsByReport[proceducres["Report name"]] = {"parameters": parametersByName,
                                                           "sp_name": proceducres["sp_name"],
                                                           "Run on": proceducres["Run on"]}
        reportsByCategory[categoryreports["Category"]] = {"reports": detailsByReport}
    outputObject = {}
    categories = reportsByCategory.keys()
    if isBeforeRun == 0:
        timestr = time.strftime("%Y_%m_%d_%H_%M_%S")
        destinationsub = (os.getcwd() + '//mismatch//' + timestr)
        destination = (destinationsub + '//')
        os.mkdir(os.getcwd() + '//mismatch//' + timestr + '//')

    primarydbdetails = dbdetails["primary"]
    rptdbdetails = dbdetails["replication"]
    primaryconn = pyodbc.connect(
        'DRIVER={SQL Server Native Client 11.0};SERVER=' + primarydbdetails[
            "server"] + ';DATABASE=' + primarydbdetails["name"] + ';UID=' + primarydbdetails[
            "username"] + ';PWD=' + get_decrypted_password(primarydbdetails["password"]))
    rptconn = pyodbc.connect(
        'DRIVER={SQL Server Native Client 11.0};SERVER=' + rptdbdetails[
            "server"] + ';DATABASE=' + rptdbdetails["name"] + ';UID=' + rptdbdetails[
            "username"] + ';PWD=' + get_decrypted_password(rptdbdetails["password"]))
    try:
        for categoryreportsTest in json_data1:
            if categoryreportsTest["Category"] in categories:
                # if categoryreportsTest["Category"] == "V2":
                    outputObject[categoryreportsTest["Category"]] = {}
                    reports = reportsByCategory[categoryreportsTest["Category"]]["reports"].keys()
                    for proceducresTest in categoryreportsTest["reports"]:
                        if proceducresTest["Report name"] in reports:
                            outputObject[categoryreportsTest["Category"]][proceducresTest["Report name"]] = {}
                            reportDetails = reportsByCategory[categoryreportsTest["Category"]]["reports"][
                                proceducresTest["Report name"]]
                            masterParams = reportDetails["parameters"]
                            masterSpname = reportDetails["sp_name"]
                            conn = primaryconn
                            if reportDetails["Run on"] == "0":
                                conn = rptconn
                            for proceducreCasesTest in proceducresTest["Test case"]:
                                # if proceducreCasesTest["test_rail_test_case_id"] == "C2954201":
                                    test_rail_id = proceducreCasesTest["test_rail_test_case_id"]
                                    outputObject[categoryreportsTest["Category"]][proceducresTest["Report name"]][
                                        test_rail_id] = {}
                                    sql = "set nocount on; exec " + masterSpname + " "
                                    sqlPrepend = ""
                                    testCaseParamsByKey = {}
                                    for testCaseParam in proceducreCasesTest["parameters"]:
                                        testCaseParamKey = list(testCaseParam.keys())[0]
                                        testCaseParamsByKey[testCaseParamKey] = testCaseParam[testCaseParamKey]
                                    testCaseParamKeys = testCaseParamsByKey.keys()
                                    for masterParamKey in masterParams.keys():
                                        finalParamValue = "'NULL'"
                                        paramValue = "'NULL'"
                                        isTypeParam = 0
                                        if "type" in masterParams[masterParamKey].keys():
                                            isTypeParam = 1
                                            sqlPrepend = sqlPrepend + "\n declare " + masterParamKey + " dbo." + \
                                                         masterParams[masterParamKey]["type"]

                                        if masterParamKey in testCaseParamKeys:
                                            paramValue = testCaseParamsByKey[masterParamKey]
                                        else:
                                            if masterParamKey in globalParamKeys:
                                                paramValue = globalPararmVals[masterParamKey]
                                            else:
                                                if "global_mapper" in masterParams[masterParamKey].keys():
                                                    globalMapper = masterParams[masterParamKey]["global_mapper"]
                                                    if globalMapper in globalParamKeys:
                                                        paramValue = globalPararmVals[globalMapper]
                                        if isTypeParam == 1 and isinstance(paramValue, list):
                                            for paramVal in paramValue:
                                                sqlPrepend = sqlPrepend + "\n insert into " + masterParamKey + " values (" + str(
                                                    paramVal) + ")"

                                        if isTypeParam == 1:
                                            finalParamValue = masterParamKey
                                        else:
                                            finalParamValue = paramValue
                                        sql = sql + " " + masterParamKey + "=" + finalParamValue + ","
                                    sql = sqlPrepend + "\n " + sql
                                    Sqlslice = slice(0, len(sql) - 1)
                                    sql = sql[Sqlslice]

                                    r = " "
                                    b = datetime.now()
                                    a = datetime.now()
                                    try:
                                        cursor = conn.cursor()
                                        a = datetime.now()
                                        try:
                                            result = conn.execute(sql)
                                            D = "pass"
                                        except Exception as failure:
                                            D = "Fail"
                                            raise Exception(failure)
                                        b = datetime.now()
                                        c = (b - a).total_seconds()
                                        r = "{";
                                        resultSetCount = 1
                                        isnext = True
                                        while (isnext):
                                            data = result.fetchall()
                                            columns = [column[0] for column in result.description]
                                            results = []

                                            for row in data:
                                                results.append(dict(zip(columns, row)))
                                            r = r + "\"ResultSet" + str(resultSetCount) + "\":" + json.dumps(results,
                                                                                                             default=str) + ","
                                            isnext = result.nextset()

                                            resultSetCount = resultSetCount + 1
                                        cursor.close()
                                    except Exception as failure:
                                        logMessage = proceducresTest["Report name"] + "(" + categoryreportsTest[
                                            "Category"] + ")" + "(" + test_rail_id + ")"
                                        logMessage = logMessage + '\n ' + sql
                                        logMessage = logMessage + '\n ' + json.dumps(failure, default=str)
                                        logging.info(logMessage)
                                        # D = "Fail"
                                    rslice = slice(0, len(r) - 1)
                                    r = r[rslice]
                                    if len(r) > 0:
                                        r = r + "}"
                                    else:
                                        r = "{}"
                                    c = (b - a).total_seconds()
                                    if runtype == 0 or runtype == 2:
                                        if statusDetails["IsBeforeRun"] == 0:
                                            write_date_to_file(
                                                os.getcwd() + '//match2//' + masterSpname + '_' + test_rail_id + '.json', r)
                                        else:
                                            write_date_to_file(
                                                os.getcwd() + '//match//' + masterSpname + '_' + test_rail_id + '.json', r)
                                        # compare
                                        if statusDetails["IsBeforeRun"] == 0:
                                            # read before file
                                            if os.path.exists(
                                                    os.getcwd() + '//match//' + masterSpname + '_' + test_rail_id + '.json'):
                                                with open(
                                                        os.getcwd() + '//match//' + masterSpname + '_' + test_rail_id + '.json',
                                                        'r') as beforeRun_data:
                                                    beforeData = json.load(beforeRun_data)
                                            # read after file
                                            if os.path.exists(
                                                    os.getcwd() + '//match2//' + masterSpname + '_' + test_rail_id + '.json'):
                                                with open(
                                                        os.getcwd() + '//match2//' + masterSpname + '_' + test_rail_id + '.json',
                                                        'r') as afterRun_data:
                                                    afterData = json.load(afterRun_data)
                                            # compare code
                                            a = (my_obj_cmp(beforeData, afterData))  # gives true
                                            if a == True:
                                                if os.path.exists(
                                                        os.getcwd() + '//match//' + masterSpname + '_' + test_rail_id + '.json'):
                                                    os.remove(
                                                        os.getcwd() + '//match//' + masterSpname + '_' + test_rail_id + '.json')
                                                if os.path.exists(
                                                        os.getcwd() + '//match2//' + masterSpname + '_' + test_rail_id + '.json'):
                                                    os.remove(
                                                        os.getcwd() + '//match2//' + masterSpname + '_' + test_rail_id + '.json')
                                            else:
                                                if os.path.exists(
                                                        os.getcwd() + '//match//' + masterSpname + '_' + test_rail_id + '.json'):
                                                    source = (
                                                            os.getcwd() + '//match//' + masterSpname + '_' + test_rail_id + '.json')
                                                    dest = shutil.move(source, destination)
                                                    os.rename(dest,
                                                              destination + masterSpname + '_' + test_rail_id + '_before.json')

                                                if os.path.exists(
                                                        os.getcwd() + '//match2//' + masterSpname + '_' + test_rail_id + '.json'):
                                                    source = (
                                                            os.getcwd() + '//match2//' + masterSpname + '_' + test_rail_id + '.json')
                                                    dest = shutil.move(source, destination)
                                                    os.rename(dest,
                                                              destination + masterSpname + '_' + test_rail_id + '_after.json')

                                        outputObject[categoryreportsTest["Category"]][proceducresTest["Report name"]][
                                            test_rail_id] = {
                                            "RunSt": D,
                                            "RunDr": c,
                                            "Comp": a
                                        }
                                    else:
                                        outputObject[categoryreportsTest["Category"]][proceducresTest["Report name"]][
                                            test_rail_id] = {
                                            "RunSt": D,
                                            "RunDr": c,
                                            "Compare": a
                                        }
    except Exception as failure:
        logging.info(failure)
    finally:
        primaryconn.close()
        rptconn.close()
    outputRunJson = []
    if isBeforeRun == 1:
        for outputObjectKey in outputObject.keys():
            outputReports = []
            for outputReportKey in outputObject[outputObjectKey].keys():
                outputCases = []
                for outputTestcaseKey in outputObject[outputObjectKey][outputReportKey].keys():
                    caseRunDetails = outputObject[outputObjectKey][outputReportKey][outputTestcaseKey]
                    outputCases.append(
                        {"test_rail_id": outputTestcaseKey, "run_details": {"BRunSt": caseRunDetails["RunSt"],
                                                                            "BRunDr": caseRunDetails["RunDr"],
                                                                            "ARunSt": "",
                                                                            "ARunDr": "", "Compare": ""
                                                                            }})
                outputReports.append({"report": outputReportKey,
                                      "test_cases": outputCases})
            outputRunJson.append({"category": outputObjectKey,
                                  "reports": outputReports})

        write_date_to_file(os.getcwd() + '//config//' + 'Output.json', outputRunJson)
    else:
        newOutputCategories = []
        for outputCategory in outputdetails:
            if outputCategory["category"] in outputObject.keys():
                reports = outputObject[outputCategory["category"]].keys()
                for outputReport in outputCategory["reports"]:
                    if outputReport["report"] in reports:
                        cases = outputObject[outputCategory["category"]][outputReport["report"]].keys()
                        for outputCase in outputReport["test_cases"]:
                            if outputCase["test_rail_id"] in cases:
                                caseRunDetails = outputObject[outputCategory["category"]][outputReport["report"]][
                                    outputCase["test_rail_id"]]
                                if caseRunDetails:
                                    outputCase["run_details"]["ARunSt"] = caseRunDetails["RunSt"]
                                    outputCase["run_details"]["ARunDr"] = caseRunDetails["RunDr"]
                                    outputCase["run_details"]["Compare"] = caseRunDetails["Comp"]
        write_date_to_file(os.getcwd() + '//config//' + 'Output.json', outputdetails)
    if runtype == 0 or runtype == 1:
        if statusDetails["IsBeforeRun"] == 1:
            statusDetails["IsBeforeRun"] = 0
        else:
            statusDetails["IsBeforeRun"] = 1
        write_date_to_file(os.getcwd() + '//config//' + 'statusdetails.json', statusDetails)
    else:
        statusDetails["IsBeforeRun"] = 1
    write_date_to_file(os.getcwd() + '//config//' + 'statusdetails.json', statusDetails)
    if isBeforeRun == 0 and runtype == 0:
        source = ('error.log')
        dest = shutil.copy(source, destination)
    drop = ""
    if isBeforeRun == 0:
        try:
            dropprocs = []
            if isBeforeRun == 0:
                apn = proc["procedures"]
                primarydbdetails = dbdetails["primary"]
                rptdbdetails = dbdetails["replication"]
                primaryconn = pyodbc.connect(
                    'DRIVER={SQL Server Native Client 11.0};SERVER=' + primarydbdetails[
                        "server"] + ';DATABASE=' + primarydbdetails["name"] + ';UID=' + primarydbdetails[
                        "username"] + ';PWD=' + get_decrypted_password(primarydbdetails["password"]))
                rptconn = pyodbc.connect(
                    'DRIVER={SQL Server Native Client 11.0};SERVER=' + rptdbdetails[
                        "server"] + ';DATABASE=' + rptdbdetails["name"] + ';UID=' + rptdbdetails[
                        "username"] + ';PWD=' + get_decrypted_password(rptdbdetails["password"]))
                for a in apn:

                    conn = primaryconn
                    dbtypename = "Main"
                    if a["Run on"] == "0":
                        dbtypename = "Rpt"
                        conn = rptconn
                    r = proccheck(a["procedurename"], conn)

                    for reportname in r:
                        # drop = drop+"""	<tr >
                        # <td  style = "border: 1px solid #ddd !important;padding: 8px !important;" > """ + reportname["Val"] + """ < / td >
                        # < td style = "border: 1px solid #ddd !important;padding: 8px !important;" > """ + dbtypename + """ < / td >
                        # </tr>"""
                        drop = drop + """		<tr >
                                    <td style="border: 1px solid #ddd !important;padding: 8px !important;">""" + \
                               reportname["Val"] + """</td>

                                    <td style="border: 1px solid #ddd !important;padding: 8px !important;">""" + dbtypename + """</td>
                                </tr>"""


        finally:
            primaryconn.close()
            rptconn.close()
y=""
if isBeforeRun == 0:

    y= upload_results_to_test_rail(outputdetails, mailconfig["user_name"], get_decrypted_password(mailconfig["password"]))


HTML = ""
counthtml = ""
totalcounthtml = ""
with open(os.getcwd() + '//config//' + 'Output.json', 'r') as outputdetails_data:
    outputdetails = json.load(outputdetails_data)
Fbpascount = 0
Fbfailcount = 0
Fapastcount = 0
Fafailcount = 0
Fnofcases = 0
Fnofreports = 0
reportCountByVersion = {}
reportCountByComp = {}
passedCaseCount = 0
failedCaseCount = 0
versionName = ""
compName = ""
for outputcategory in outputdetails:
    bpascount = 0
    bfailcount = 0
    apastcount = 0
    afailcount = 0
    nofcases = 0
    acompare = 0
    anoreports = 0
    currentVersionCount = {}
    currentCompCount = {}
    if outputcategory["category"] == "V2":
        versionName = "V2"
    else:
        versionName = "V1"
    compName = outputcategory["category"]
    if versionName in reportCountByVersion.keys():
        currentVersionCount = reportCountByVersion[versionName]
    else:
        reportCountByVersion[versionName] = {
            "report_count": 0,
            "passed_case_count": 0,
            "failed_case_count": 0
        }
        currentVersionCount = reportCountByVersion[versionName]
    if compName in reportCountByComp.keys():
        currentCompCount = reportCountByComp[compName]
    else:
        reportCountByComp[compName] = {
            "report_count": 0,
            "passed_case_count": 0,
            "failed_case_count": 0
        }
        currentCompCount = reportCountByComp[compName]
    for report in outputcategory["reports"]:
        Fnofreports = Fnofreports + 1
        anoreports = anoreports + 1
        currentVersionCount["report_count"] = currentVersionCount["report_count"] + 1
        currentCompCount["report_count"] = currentCompCount["report_count"] + 1
        for testcase in report["test_cases"]:

            nofcases = nofcases + 1
            Fnofcases = Fnofcases + 1
            if testcase["run_details"]["BRunSt"] == "pass":
                bpascount = bpascount + 1
                Fbpascount = Fbpascount + 1
            if testcase["run_details"]["BRunSt"] == "Fail":
                bfailcount = bfailcount + 1
                Fbfailcount = Fbfailcount + 1
            if testcase["run_details"]["ARunSt"] == "pass":
                apastcount = apastcount + 1
                Fapastcount = Fapastcount + 1
            if testcase["run_details"]["ARunSt"] == "Fail":
                afailcount = afailcount + 1
                Fafailcount = Fafailcount + 1

            if not (testcase["run_details"]["Compare"]):
                acompare = acompare + 1
            if testcase["run_details"]["ARunSt"] == "pass":
                currentVersionCount["passed_case_count"] = currentVersionCount["passed_case_count"] + 1
                currentCompCount["passed_case_count"] = currentCompCount["passed_case_count"] + 1
                passedCaseCount = passedCaseCount + 1
            else:
                currentVersionCount["failed_case_count"] = currentVersionCount["failed_case_count"] + 1
                currentCompCount["failed_case_count"] = currentCompCount["failed_case_count"] + 1
                failedCaseCount = failedCaseCount + 1


            HTML = HTML + """		<tr>
                                			<td style="border: 1px solid #ddd !important;padding: 8px !important;">""" + \
                   outputcategory["category"] + """</td>
                                			<td style="border: 1px solid #ddd !important;padding: 8px !important;">""" + \
                   report["report"] + """</td>
                                			<td style="border: 1px solid #ddd !important;padding: 8px !important;">""" + \
                   testcase["test_rail_id"] + """</td>
                                			<td style="border: 1px solid #ddd !important;padding: 8px !important;">""" + str(
                testcase["run_details"]["BRunSt"]) + """</td>
                                			<td style="border: 1px solid #ddd !important;padding: 8px !important;">""" + str(
                testcase["run_details"]["BRunDr"]) + """</td>
                                			<td style="border: 1px solid #ddd !important;padding: 8px !important;">""" + str(
                testcase["run_details"]["ARunSt"]) + """</td>
                                			<td style="border: 1px solid #ddd !important;padding: 8px !important;">""" + str(
                testcase["run_details"]["ARunDr"]) + """</td>
                                			<td style="border: 1px solid #ddd !important;padding: 8px !important;">""" + str(
                testcase["run_details"]["Compare"]) + """</td>
                                		</tr>
                                		"""
    counthtml = counthtml + """	<tr >
            <td style="border: 1px solid #ddd !important;padding: 8px !important;">""" + outputcategory["category"] + """ </td>
            <td style="border: 1px solid #ddd !important;padding: 8px !important;">""" + str(anoreports) + """</td>
            <td style="border: 1px solid #ddd !important;padding: 8px !important;">""" + str(nofcases) + """</td>
			<td style="border: 1px solid #ddd !important;padding: 8px !important;">""" + str(bpascount) + """</td>
			<td style="border: 1px solid #ddd !important;padding: 8px !important;">""" + str(bfailcount) + """</td>
			<td style="border: 1px solid #ddd !important;padding: 8px !important;">""" + str(apastcount) + """</td>
			<td style="border: 1px solid #ddd !important;padding: 8px !important;">""" + str(afailcount) + """</td>
			<td style="border: 1px solid #ddd !important;padding: 8px !important;">""" + str(acompare) + """</td>
		</tr>"""
totalcounthtml = totalcounthtml + """		<tr >
            <td style="border: 1px solid #ddd !important;padding: 8px !important;">""" + str(Fnofreports) + """</td>
			<td style="border: 1px solid #ddd !important;padding: 8px !important;">""" + str(Fnofcases) + """</td>
			<td style="border: 1px solid #ddd !important;padding: 8px !important;">""" + str(Fbpascount) + """</td>
			<td style="border: 1px solid #ddd !important;padding: 8px !important;">""" + str(Fbfailcount) + """</td>
			<td style="border: 1px solid #ddd !important;padding: 8px !important;">""" + str(Fapastcount) + """</td>
			<td style="border: 1px solid #ddd !important;padding: 8px !important;">""" + str(Fafailcount) + """</td>
		</tr>"""

finalHtml = """<style type="text/css">
    body {
        font-family: Arial, Helvetica, sans-serif !important;
    }
</style>
<h2 style="color: #3f6172;;font-weight: bold;"> <b style="border-bottom: 2px solid #3f6172;"> Overall run details </b></h2>

<!---<div style="border: 1px solid black;padding:20px;width: max-content;"> --->
    <table style="border-collapse: collapse;padding:5px;">
        <colgroup>
            <col style="width: 40%;" />
            <col style="width: 15%;" />
            <col style="width: 10%;" />
            <col style="width: 15%;" />
            <col style="width: 10%;" />
            <col style="width: 10%;" />
        </colgroup>
        <tbody>
            <tr style="border: 1px solid #3f6172 !important;">
                <td
                    style="background-color: #3f6172; color: white;font-size:17px;font-weight:bold;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;text-align:general;vertical-align:bottom;border-right:2px solid #bbb !important;border-top: 1px solid #3f6172;border-left: 1px solid #3f6172;padding: 5px;">
                    Version</td>
                <td
                    style="background-color: #3f6172; color: white;font-size:17px;font-weight:bold;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;text-align:general;vertical-align:bottom;border-right:2px solid #bbb !important; border-top: 1px solid #3f6172;padding: 5px;">
                    Test Type</td>
                <td
                    style="background-color: #3f6172; color: white;font-size:17px;font-weight:bold;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;text-align:general;vertical-align:bottom;border-right:2px solid #bbb !important;border-top: 1px solid #3f6172;padding: 5px;">
                    Total</td>
                <td
                    style="background-color: #3f6172; color: white;font-size:17px;font-weight:bold;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;text-align:general;vertical-align:bottom;border-right:2px solid #bbb !important;border-top: 1px solid #3f6172;padding: 5px;">
                    Passed</td>
                <td
                    style="background-color: #3f6172; color: white;font-size:17px;font-weight:bold;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;text-align:general;vertical-align:bottom;border-right:2px solid #bbb !important;border-top: 1px solid #3f6172;padding: 5px;">
                    Failed</td>
                <td
                    style="background-color: #3f6172; color: white;font-size:17px;font-weight:bold;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;text-align:general;vertical-align:bottom;border-top: 1px solid #3f6172;padding: 5px;">
                    Skipped</td>
            </tr>"""
for rptVersion in reportCountByVersion.keys():
    rptVersionCount = reportCountByVersion[rptVersion]
    finalHtml = finalHtml + """
            <tr style="border: 1px solid #fff !important;">
                <td
                    style="color:black;font-size:15px;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;text-align:general;vertical-align:bottom;border:1px solid #bbb !important;border-top:none;padding: 5px;">
                    """+ rptVersion +""" (Number of reports : """+ str(rptVersionCount["report_count"]) +""")</td>
                <td
                    style="color:black;font-size:15px;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;text-align:general;vertical-align:bottom;border:1px solid #bbb !important;border-top:none;border-left:none;padding: 5px;">
                    Funtional</td>
                <td
                    style="color:black;font-size:15px;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;text-align:right;vertical-align:bottom;border:1px solid #bbb !important;border-top:none;border-left:none;padding: 5px;">
                    """+ str(rptVersionCount["passed_case_count"] + rptVersionCount["failed_case_count"]) +"""</td>
                <td
                    style="color:black;font-size:15px;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;text-align:right;vertical-align:bottom;border:1px solid #bbb !important;border-top:none;border-left:none;padding: 5px;">
                    """+ str(rptVersionCount["passed_case_count"]) +"""</td>
                <td
                    style="color:black;font-size:15px;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;text-align:right;vertical-align:bottom;border:1px solid #bbb !important;border-top:none;border-left:none;padding: 5px;">
                    """+ str(rptVersionCount["failed_case_count"]) +"""</td>
                <td
                    style="color:black;font-size:15px;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;text-align:right;vertical-align:bottom;border:1px solid #bbb !important;border-top:none;border-left:none;padding: 5px;">
                    &nbsp;</td>
            </tr>"""
totalCaseCount = passedCaseCount + failedCaseCount
roundDecimals = 2
passedCasePerc = round((passedCaseCount/totalCaseCount) * 100, roundDecimals)
failedCasePerc = round((failedCaseCount/totalCaseCount) * 100, roundDecimals)
failedCellBGColorCss = ""
if failedCasePerc > 0:
    if failedCasePerc > passedCasePerc:
        failedCellBGColorCss = "background-color: red"
    else:
        failedCellBGColorCss = "background-color: Yellow"
finalHtml = finalHtml + """
            <tr style="border: 1px solid #fff !important;">
                <td colspan="2"
                    style="color:black;font-size:15px;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;text-align:general;vertical-align:bottom;border:1px solid #bbb;border-top:none;padding: 5px;">
                    Summary</td>
                <td
                    style="color:black;font-size:15px;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;text-align:right;vertical-align:bottom;border:1px solid #bbb;border-top:none;border-left:none;padding: 5px;">
                    """+ str(totalCaseCount) +"""</td>
                <td
                    style="color:black;background-color:limegreen;font-size:15px;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;text-align:right;vertical-align:bottom;border:1px solid #bbb;border-top:none;border-left:none;padding: 5px;">
                    """+ str(passedCaseCount) +"""("""+ str(passedCasePerc) +"""%)</td>
                <td
                    style="color:black;"""+ failedCellBGColorCss +""";font-size:15px;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;text-align:right;vertical-align:bottom;border:1px solid #bbb;border-top:none;border-left:none;padding: 5px;">
                    """+ str(failedCaseCount) +"""("""+ str(failedCasePerc) +"""%)</td>
                <td
                    style="color:black;font-size:15px;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;text-align:right;vertical-align:bottom;border:1px solid #bbb;border-top:none;border-left:none;padding: 5px;">
                    &nbsp;</td>
            </tr>
        </tbody>
    </table> 
<!----</div> ---->

<h2 style="color: #3f6172;font-weight: bold;"> <b style="border-bottom: 2px solid #3f6172;"> Module wise Test run details </b> </h2>

<!-----<div style="border: 1px solid black;padding:20px;width: max-content;"> ---->
    <table style="border-collapse: collapse;padding:5px;">
        <colgroup>
            <col style="width: 40%;" />
            <col style="width: 15%;" />
            <col style="width: 10%;" />
            <col style="width: 15%;" />
            <col style="width: 10%;" />
            <col style="width: 10%;" />
        </colgroup>
        <tbody>
            <tr style="border: 1px solid #3f6172 !important;">
                <td
                    style="background-color: #3f6172; color: white;font-size:17px;font-weight:bold;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;text-align:general;vertical-align:bottom;border-right:2px solid #bbb !important;border-top: 1px solid #3f6172;border-left: 1px solid #3f6172;padding: 5px;">
                    Component</td>
                <td
                    style="background-color: #3f6172; color: white;font-size:17px;font-weight:bold;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;text-align:general;vertical-align:bottom;border-right:2px solid #bbb !important; border-top: 1px solid #3f6172;padding: 5px;">
                    Test Type</td>
                <td
                    style="background-color: #3f6172; color: white;font-size:17px;font-weight:bold;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;text-align:general;vertical-align:bottom;border-right:2px solid #bbb !important;border-top: 1px solid #3f6172;padding: 5px;">
                    Total</td>
                <td
                    style="background-color: #3f6172; color: white;font-size:17px;font-weight:bold;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;text-align:general;vertical-align:bottom;border-right:2px solid #bbb !important;border-top: 1px solid #3f6172;padding: 5px;">
                    Passed</td>
                <td
                    style="background-color: #3f6172; color: white;font-size:17px;font-weight:bold;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;text-align:general;vertical-align:bottom;border-right:2px solid #bbb !important;border-top: 1px solid #3f6172;padding: 5px;">
                    Failed</td>
                <td
                    style="background-color: #3f6172; color: white;font-size:17px;font-weight:bold;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;text-align:general;vertical-align:bottom;border-top: 1px solid #3f6172;padding: 5px;">
                    Skipped</td>
            </tr>"""
for rptComp in reportCountByComp.keys():
    rptCompCount = reportCountByComp[rptComp]
    failedCompCaseCount = rptCompCount["failed_case_count"]
    passedCompCaseCount = rptCompCount["passed_case_count"]
    totalCompCaseCount = passedCompCaseCount + failedCompCaseCount
    passedCompCasePerc = round((passedCompCaseCount/totalCompCaseCount) * 100, roundDecimals)
    failedCompCasePerc = round((failedCompCaseCount/totalCompCaseCount) * 100, roundDecimals)
    failedCompCellBGColorCss = ""
    if failedCompCasePerc > 0:
        if failedCompCasePerc > passedCompCasePerc:
            failedCompCellBGColorCss = "background-color: red"
        else:
            failedCompCellBGColorCss = "background-color: yellow"
    finalHtml = finalHtml + """
            <tr style="border: 1px solid #fff !important;">
                <td rowspan="2"
                    style="color:black;font-size:15px;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;text-align:general;vertical-align:bottom;border:1px solid #bbb;border-top:none;padding: 5px;">
                    """+ rptComp +""" (Number of reports: """+ str(rptCompCount["report_count"]) +""")</td>
                <td
                    style="color:black;font-size:15px;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;text-align:general;vertical-align:bottom;border:1px solid #bbb;border-top:none;border-left:none;padding: 5px;">
                    Funtional</td>
                <td
                    style="color:black;font-size:15px;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;text-align:right;vertical-align:bottom;border:1px solid #bbb;border-top:none;border-left:none;padding: 5px;">
                    """+ str(totalCompCaseCount) +"""</td>
                <td
                    style="color:black;font-size:15px;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;text-align:right;vertical-align:bottom;border:1px solid #bbb;border-top:none;border-left:none;padding: 5px;">
                    """+ str(passedCompCaseCount) +"""</td>
                <td
                    style="color:black;font-size:15px;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;text-align:right;vertical-align:bottom;border:1px solid #bbb;border-top:none;border-left:none;padding: 5px;">
                    """+ str(failedCompCaseCount) +"""</td>
                <td
                    style="color:black;font-size:15px;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;text-align:right;vertical-align:bottom;border:1px solid #bbb;border-top:none;border-left:none;padding: 5px;">
                    &nbsp;</td>
            </tr>
            <tr style="border: 1px solid #fff !important;">
                <td
                    style="color:black;font-size:15px;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;text-align:general;vertical-align:bottom;border:1px solid #bbb;border-top:none;border-left:none;padding: 5px;">
                    Total</td>
                <td
                    style="color:black;font-size:15px;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;text-align:right;vertical-align:bottom;border:1px solid #bbb;border-top:none;border-left:none;padding: 5px;">
                    """+ str(totalCompCaseCount) +"""</td>
                <td
                    style="color:black;background-color:limegreen;font-size:15px;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;text-align:right;vertical-align:bottom;border:1px solid #bbb;border-top:none;border-left:none;padding: 5px;">
                    """+ str(passedCompCaseCount) +"""("""+ str(passedCompCasePerc) +"""%)</td>
                <td
                    style="color:black;"""+ failedCompCellBGColorCss +""";font-size:15px;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;text-align:right;vertical-align:bottom;border:1px solid #bbb;border-top:none;border-left:none;padding: 5px;">
                    """+ str(failedCompCaseCount) +"""("""+ str(failedCompCasePerc) +"""%)</td>
                <td
                    style="color:black;font-size:15px;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri, sans-serif;text-align:right;vertical-align:bottom;border:1px solid #bbb;border-top:none;border-left:none;padding: 5px;">
                    &nbsp;</td>
            </tr>"""
finalHtml = finalHtml + """
        </tbody>
    </table>
<!----</div> ---->"""

HTML1 = """<html>
<head>
<style>
.table {
  font-family: Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

.table td, .table th {
  border: 1px solid #f2f2f2 !important;
  padding: 8px !important;
}
.tfont-family: Arial, Helvetica, sans-serif;able th {
  padding-top: 12px !important;
  padding-bottom: 12px !important;
  text-align: left;
  background-color: Gray;
  color: white;
	width: 10%;
}
</style>
</head>
<p>All</p>
<p>Please find the Automation Results below.</p>
"""
HTML2 = """
<p style="font-size: 18px; color: #2A9DD7">Module wise test run details</p>
<div style="overflow-x:auto;">
<table class="table" style=" border-collapse: collapse;">
	<tbody>
		<tr >
			<th style="border: 1px solid #f2f2f2 !important;padding: 8px 8px 8px 8px !important;background-color: #3f6172;color: #fff;font-size:14px;">Module</th>
			<th style="border: 1px solid #f2f2f2 !important;padding: 8px 8px 8px 8px !important;background-color: #3f6172;color: #fff;font-size:14px;">Reports</th>
			<th style="border: 1px solid #f2f2f2 !important;padding: 8px 8px 8px 8px !important;background-color: #3f6172;color: #fff;font-size:14px;">Scenarios</th>
			<th style="border: 1px solid #f2f2f2 !important;padding: 8px 8px 8px 8px !important;background-color: #3f6172;color: #fff;font-size:14px;">Pre-build Pass</th>
			<th style="border: 1px solid #f2f2f2 !important;padding: 8px 8px 8px 8px !important;background-color: #3f6172;color: #fff;font-size:14px;">Pre-build Failed</th>
			<th style="border: 1px solid #f2f2f2 !important;padding: 8px 8px 8px 8px !important;background-color: #3f6172;color: #fff;font-size:14px;">Post-build Pass</th>
			<th style="border: 1px solid #f2f2f2 !important;padding: 8px 8px 8px 8px !important;background-color: #3f6172;color: #fff;font-size:14px;">Post-build Fail</th>
			<th style="border: 1px solid #f2f2f2 !important;padding: 8px 8px 8px 8px !important;background-color: #3f6172;color: #fff;font-size:14px;">Comparision Failure</th>
		</tr>"""
HTML3 = """	</tbody>
</table>
</div>
<div>&nbsp;</div>
"""
HTML7 = """
<br/>
<a href="""+y+""" target="_blank">Click here for detailed test report</a>
<p>The error log and test cases detailed report(Including procedure runtime) is attached to this email as a ZIP file.</p>
<p class="small">
Sincerely,<br>
Zenoti Product Quality Management.<br>
</p>
<div>&nbsp;</div>
"""
HTML4 = """
<p style="font-size: 18px; color: #2A9DD7">Case wise Run details</p>
<div style="overflow-x:auto;">
<table class="table" style=" border-collapse: collapse;">
	<tbody>
		<tr>
			<th style="border: 1px solid #f2f2f2 !important;padding: 8px 8px 8px 8px !important;background-color: #3f6172;color: #fff;font-size:14px;">Category</th>
			<th style="border: 1px solid #f2f2f2 !important;padding: 8px 8px 8px 8px !important;background-color: #3f6172;color: #fff;font-size:14px;">Report name</th>
			<th style="border: 1px solid #f2f2f2 !important;padding: 8px 8px 8px 8px !important;background-color: #3f6172;color: #fff;font-size:14px;">Testrail_ID</th>
			<th style="border: 1px solid #f2f2f2 !important;padding: 8px 8px 8px 8px !important;background-color: #3f6172;color: #fff;font-size:14px;">Pre-build Status</th>
			<th style="border: 1px solid #f2f2f2 !important;padding: 8px 8px 8px 8px !important;background-color: #3f6172;color: #fff;font-size:14px;">Pre-build Runtime</th>
			<th style="border: 1px solid #f2f2f2 !important;padding: 8px 8px 8px 8px !important;background-color: #3f6172;color: #fff;font-size:14px;">Post-build Status</th>
			<th style="border: 1px solid #f2f2f2 !important;padding: 8px 8px 8px 8px !important;background-color: #3f6172;color: #fff;font-size:14px;">Post-build Runtime</th>
			<th style="border: 1px solid #f2f2f2 !important;padding: 8px 8px 8px 8px !important;background-color: #3f6172;color: #fff;font-size:14px;">comparison status</th>
		</tr>
"""
HTML5 = """	</tbody>
</table>
</div>
</html>
"""
HTML6 = """
<p style="font-size: 18px; color: #2A9DD7">Overall run details</p>
<div style="overflow-x:auto;">
<table class="table" style=" border-collapse: collapse;">
	<tbody>
		<tr >
		    <th style="border: 1px solid #f2f2f2 !important;padding: 8px 8px 8px 8px !important;background-color: #3f6172;color: #fff;font-size:14px;">Reports</th>
			<th style="border: 1px solid #f2f2f2 !important;padding: 8px 8px 8px 8px !important;background-color: #3f6172;color: #fff;font-size:14px;">Scenarios</th>
			<th style="border: 1px solid #f2f2f2 !important;padding: 8px 8px 8px 8px !important;background-color: #3f6172;color: #fff;font-size:14px;">Pre-build Pass</th>
			<th style="border: 1px solid #f2f2f2 !important;padding: 8px 8px 8px 8px !important;background-color: #3f6172;color: #fff;font-size:14px;">Pre-build Fail</th>
			<th style="border: 1px solid #f2f2f2 !important;padding: 8px 8px 8px 8px !important;background-color: #3f6172;color: #fff;font-size:14px;">Post-build Pass</th>
			<th style="border: 1px solid #f2f2f2 !important;padding: 8px 8px 8px 8px !important;background-color: #3f6172;color: #fff;font-size:14px;">Post-build Fail</th>

		</tr>
"""
HTML8 = """
<p style="font-size: 18px; color: #2A9DD7">Missing procedures</p>
<div style="overflow-x:auto;">
<table class="table" style=" border-collapse: collapse;">
	<tbody>
		<tr >
			<th style="border: 1px solid #f2f2f2 !important;padding: 8px 8px 8px 8px !important;background-color: #3f6172;color: #fff;font-size:14px;">Procedurename</th>
			<th style="border: 1px solid #f2f2f2 !important;padding: 8px 8px 8px 8px !important;background-color: #3f6172;color: #fff;font-size:14px;">Database</th>

		</tr>"""

View = HTML1 + finalHtml + HTML7
# print(View)
Thtml = HTML4 + HTML + HTML5
if isBeforeRun == 0:
    thtml = HTML4 + HTML + HTML5
    # empty list
    data = []

    # for getting the header from
    # the HTML file
    list_header = []
    soup = BeautifulSoup(thtml, 'html.parser')
    header = soup.find_all("table")[0].find("tr")

    for items in header:
        try:
            list_header.append(items.get_text())
        except:
            continue

    # for getting the data
    HTML_data = soup.find_all("table")[0].find_all("tr")[1:]

    for element in HTML_data:
        sub_data = []
        for sub_element in element:
            try:
                sub_data.append(sub_element.get_text())
            except:
                continue
        data.append(sub_data)

    # Storing the data into Pandas
    # DataFrame
    dataFrame = pd.DataFrame(data=data, columns=list_header)

    # Converting Pandas DataFrame
    # into CSV file
    dataFrame.to_csv('Testrundetails.csv')
    source = ('Testrundetails.csv')
    dest = shutil.move(source, destination)
    shutil.make_archive(destination, 'zip', destination)
    shutil.rmtree(destination)

if runtype == 2:
    email_report(View, 'Reports-Automation',
                 {"EmailSender": mailconfig["EmailSender"], "EmailRecepients": mailconfig["EmailRecepients"],
                  "EmailSenderAlias": mailconfig["EmailSenderAlias"],
                  "EmailSenderpassword": mailconfig["EmailSenderpassword"]}, global_file_name='error.log')
    file = open("error.log", "r+")
    file.truncate(0)
    file.close()

elif isBeforeRun == 0:
    email_report(View, 'Reports Automation Run Details' + ' - ' + str(datetime.now().strftime("%Y%m%d%H%M")),
                 {"EmailSender": mailconfig["EmailSender"], "EmailRecepients": mailconfig["EmailRecepients"],
                  "EmailSenderAlias": mailconfig["EmailSenderAlias"],
                  "EmailSenderpassword": mailconfig["EmailSenderpassword"]}, global_file_name=destinationsub + '.zip')
    file = open("error.log", "r+")
    file.truncate(0)
    file.close()
    os.remove(destinationsub + '.zip')

# if isBeforeRun == 0:
#     y= upload_results_to_test_rail(outputdetails, mailconfig["user_name"], get_decrypted_password(mailconfig["password"]))
#     print(y)
# if isBeforeRun == 0:
#     email_report(HTML8 , 'Missing procedures' + ' - ' + str(datetime.now().strftime("%Y%m%d%H%M")),
#                  {"EmailSender": mailconfig["EmailSender"], "EmailRecepients": mailconfig["EmailRecepients"],
#                   "EmailSenderAlias": mailconfig["EmailSenderAlias"],
#                   "EmailSenderpassword": mailconfig["EmailSenderpassword"]}, global_file_name='error.log')
#
