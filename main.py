import logging
import datetime
import os
from testrail import APIClient

def create_run(client, case_list, project_id, suite_id, run_name):
    try:
        case_ids = []
        for test_case_id in case_list.keys():
            case_ids.append(int(test_case_id))
        run = client.send_post('add_run/' + str(project_id),
                               {
                                   'suite_id': suite_id,
                                   'name': run_name,
                                   'include_all': False,
                                   'case_ids': case_ids
                               }
                               )
        return run
    except ConnectionError as e:
        logging.error(e)
        return False
    except TimeoutError as e:
        logging.error(e)
        return False
    except Exception as e:
        logging.error(e)
        return False

def update_test_run_results(client, run_id, test_run_test_cases, run_data):
    try:
        results = list()
        # tc_comment = "This test has been passed without any errors" if test_result[
        #                                                                        1] is "Passed" else "This test case has been failed"
        for test_run_case in run_data.keys():
            case_id = test_run_case
            run_case_id = -1
            run_case_status = 1 if  run_data[test_run_case] == "pass" else 5
            for data in test_run_test_cases:
                if data['case_id'] ==  case_id:
                    run_case_id = data['id']
            results.append({
                "test_id": run_case_id,
                "status_id": run_case_status,
                "comment": 'This run is created by automation'
            }
            )

        client.send_post('add_results/' + str(run_id),
                         {
                             'results': results
                         })
        return True
    except Exception as e:
        logging.error(e)
        return False

def upload_results_to_test_rail(test_rail_results_map, user_name, password):
    test_rail_url = 'https://zenoti.testrail.com/'
    test_rail_user_name = user_name
    test_rail_user_password = password
    client = APIClient(test_rail_url)
    client.user = test_rail_user_name
    client.password = test_rail_user_password

    test_run_test_case_ids_map = dict()
    for category in test_rail_results_map:
        reports = category['reports']
        for test_case in reports:
            for test_step in test_case['test_cases']:
                test_run_test_case_ids_map[int(test_step['test_rail_id'].replace('C',''))] =  test_step['run_details']['ARunSt']
    run_id = None
    project_id_ = None
    milestone_id = None
    suite_id = None
    section_id = None
    test_rail_integration_parameters = read_config(os.getcwd() + "\\testrail\\testrail_integration_parameters.json")
    project_name = test_rail_integration_parameters['project']
    suite_name = test_rail_integration_parameters['suite']
    project_list = client.send_get('get_projects')
    for project in project_list:
        if project[u'name'] == project_name:
            project_id_ = project[u'id']
            break
    suite_list = client.send_get('get_suites/' + str(project_id_))
    for suite in suite_list:
        if suite[u'name'] == suite_name:
            suite_id = suite[u'id']
    run = create_run(client, test_run_test_case_ids_map, project_id_, suite_id,'REPORTS AUTOMATION RUN')
    run_id = run['id']
    run_url = run['url']
    test_run_test_cases = client.send_get('get_tests/' + str(run_id))
    update_test_run_results(client,run_id, test_run_test_cases, test_run_test_case_ids_map)

    return run_url

def read_config(config_file_location):
    try:
        import json
        json_data = None
        with open(config_file_location, 'r') as config_loc:
            try:
                json_data = json.load(config_loc)
            except Exception:
                logging.error("Configuration " + config_file_location + " not in a valid json format")
        return json_data
    except:
        import traceback
        logging.error("Exception: Could not read the config file at the specified path")
        print(traceback.format_exc())
        raise







